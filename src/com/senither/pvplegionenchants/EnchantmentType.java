package com.senither.pvplegionenchants;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.Material;

public enum EnchantmentType
{

    GLOBAL("global", null),
    SWORD("sword", null),
    BOW("bow", null),
    WEAPON("weapon", Arrays.asList(SWORD, BOW)),
    BOOTS("boots", null),
    LEGGINGS("leggings", null),
    CHESTPLATE("chestplate", null),
    HEALMET("healmet", null),
    ARMOR("armor", Arrays.asList(BOOTS, LEGGINGS, CHESTPLATE, HEALMET)),
    PICKAXE("pickaxe", null),
    SHOVEL("shovel", null),
    AXE("axe", null),
    TOOLS("tools", Arrays.asList(PICKAXE, SHOVEL, AXE));

    private final String name;
    private final List<EnchantmentType> child;
    private static final Map<String, EnchantmentType> map = new HashMap<>();

    static {
        for (EnchantmentType p : values()) {
            map.put(p.name.toLowerCase(), p);
        }
    }

    private EnchantmentType(String name, List<EnchantmentType> child)
    {
        this.name = name;
        this.child = child;
    }

    /**
     * @return the child
     */
    public List<EnchantmentType> getChild()
    {
        return child;
    }

    public String getName()
    {
        return name;
    }

    public static EnchantmentType getTypeByName(String name)
    {
        if (map.containsKey(name.toLowerCase())) {
            return map.get(name.toLowerCase());
        }
        return null;
    }

    public static EnchantmentType getType(Material material)
    {
        if (material.isBlock()) {
            return null;
        }

        switch (material) {
            case WOOD_SWORD:
            case STONE_SWORD:
            case GOLD_SWORD:
            case IRON_SWORD:
            case DIAMOND_SWORD:
                return EnchantmentType.SWORD;
            case BOW:
                return EnchantmentType.BOW;
            case LEATHER_BOOTS:
            case CHAINMAIL_BOOTS:
            case IRON_BOOTS:
            case GOLD_BOOTS:
            case DIAMOND_BOOTS:
                return EnchantmentType.BOOTS;
            case LEATHER_LEGGINGS:
            case CHAINMAIL_LEGGINGS:
            case IRON_LEGGINGS:
            case GOLD_LEGGINGS:
            case DIAMOND_LEGGINGS:
                return EnchantmentType.LEGGINGS;
            case LEATHER_CHESTPLATE:
            case CHAINMAIL_CHESTPLATE:
            case IRON_CHESTPLATE:
            case GOLD_CHESTPLATE:
            case DIAMOND_CHESTPLATE:
                return EnchantmentType.CHESTPLATE;
            case LEATHER_HELMET:
            case CHAINMAIL_HELMET:
            case IRON_HELMET:
            case GOLD_HELMET:
            case DIAMOND_HELMET:
                return EnchantmentType.HEALMET;
            case STONE_PICKAXE:
            case IRON_PICKAXE:
            case DIAMOND_PICKAXE:
                return EnchantmentType.PICKAXE;
            case STONE_SPADE:
            case IRON_SPADE:
            case DIAMOND_SPADE:
                return EnchantmentType.SHOVEL;
            case STONE_AXE:
            case IRON_AXE:
            case DIAMOND_AXE:
                return EnchantmentType.AXE;
            default:
                return null;
        }
    }
}