package com.senither.pvplegionenchants.utils;

public class Permissions
{

    private static final String MAIN = "legionenchants.";

    public static final String USE = MAIN + "use";
    public static final String LIST = MAIN + "list";
    public static final String RELOAD = MAIN + "reload";
}
