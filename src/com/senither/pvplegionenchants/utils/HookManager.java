package com.senither.pvplegionenchants.utils;

import com.senither.pvplegionenchants.PvPLegionEnchants;
import java.util.Map.Entry;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;

public class HookManager
{

    private static WhatIsItHook whatIsIt = null;

    public static void register()
    {
        if (PvPLegionEnchants.getInstance().isDebugEnabled()) {
            PvPLegionEnchants.getInstance().getChatManager().getLogger().info("Hook - Starting Hook manager");
        }

        registerWhatIsIt();
    }

    public static void registerWhatIsIt()
    {
        if (PvPLegionEnchants.getInstance().isDebugEnabled()) {
            PvPLegionEnchants.getInstance().getChatManager().getLogger().info("Hook - Searching for WhatIsIt..");
        }

        PluginManager pm = PvPLegionEnchants.getInstance().getServer().getPluginManager();

        if (!pm.isPluginEnabled("WhatIsIt")) {
            if (PvPLegionEnchants.getInstance().isDebugEnabled()) {
                PvPLegionEnchants.getInstance().getChatManager().getLogger().info("Hook - Faild to find WhatIsIt, canceling..");
            }
            return;
        }

        if (PvPLegionEnchants.getInstance().isDebugEnabled()) {
            PvPLegionEnchants.getInstance().getChatManager().getLogger().info("Hook - WhatIsIt was found and is enabled, registering hook manager..");
        }

        whatIsIt = new WhatIsItHook();
    }

    public static String getItemName(ItemStack item)
    {
        if (whatIsIt == null) {
            return null;
        }
        return whatIsIt.itemName(item);
    }

    public static String getEntityName(Entity entity)
    {
        if (whatIsIt == null) {
            return null;
        }
        return whatIsIt.entityName(entity);
    }

    public static String getBlockName(Block block)
    {
        if (whatIsIt == null) {
            return null;
        }
        return whatIsIt.blockName(block);
    }

    public static String getEnchantmentName(Entry<Enchantment, Integer> enchantment)
    {
        if (whatIsIt == null) {
            return null;
        }
        return whatIsIt.enchantmentName(enchantment);
    }

    public static boolean isHookEnabled(HookType type)
    {
        switch (type) {
            case WHATISIT:
                return whatIsIt != null;
        }
        return false;
    }

    public enum HookType
    {

        WHATISIT;
    }
}
