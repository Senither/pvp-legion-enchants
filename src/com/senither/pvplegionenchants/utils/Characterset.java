package com.senither.pvplegionenchants.utils;

import java.util.Random;

public class Characterset
{

    private static final Random random = new Random();
    private static final char[] numeric = "0123456789".toCharArray();
    private static final char[] alphabetical = "qwertyuiopasdfghjklzxcvbnm".toCharArray();
    private static final char[] alphanumeric = "qwertyuiopasdfghjklzxcvbnm0123456789".toCharArray();
    private static final char[] characters = "!\"#%&/()=?{}[]^*-_.:,;<>@£$".toCharArray();
    private static final char[] global = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789!\"#%&/()=?{}[]^*-_.:,;<>@£$".toCharArray();

    public static String generate(int length)
    {
        return generate(CharactersetType.GLOBAL, length);
    }

    public static String generate(CharactersetType type)
    {
        return generate(type, 32);
    }

    public static String generate(CharactersetType type, int length)
    {
        if (length <= 0) {
            return null;
        }

        char[] chars = getCharset(type);

        if (chars == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < length; i++) {
            sb.append(chars[random.nextInt(chars.length)]);
        }

        return sb.toString();
    }

    private static char[] getCharset(CharactersetType type)
    {
        switch (type) {
            case NUMERIC:
                return numeric;
            case ALPHABETICAL:
                return alphabetical;
            case ALPHANUMERIC:
                return alphanumeric;
            case CHARACTERS:
                return characters;
            case GLOBAL:
                return global;
            default:
                return null;
        }
    }

    public enum CharactersetType
    {

        NUMERIC, ALPHABETICAL, ALPHANUMERIC, CHARACTERS, GLOBAL;
    }
}
