package com.senither.pvplegionenchants.utils;

import com.senither.pvplegionenchants.Enchantments;
import com.senither.pvplegionenchants.PvPLegionEnchants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import org.bukkit.configuration.file.FileConfiguration;

public class Messages
{

    private static final HashMap<String, Double> modifiers = new HashMap<>();
    private static final HashMap<String, List<String>> messages = new HashMap<>();

    public static void register(PvPLegionEnchants plugin)
    {
        clear();
        FileConfiguration config = plugin.getConfig();

        for (Enchantments enchantment : Enchantments.values()) {
            String name = formatName(enchantment.getName());

            if (!config.contains("enchantments." + name + "")) {
                if (PvPLegionEnchants.getInstance().isDebugEnabled()) {
                    PvPLegionEnchants.getInstance().getLogger().info("Failed to find the enchant at the following path:");
                    PvPLegionEnchants.getInstance().getLogger().log(Level.INFO, "Path: enchantments.{0}", name);
                }
                continue;
            }
            modifiers.put(name, config.getDouble("enchantments." + name + ".modifier", 2.5D));
            messages.put(name, config.getStringList("enchantments." + name + ".message"));
        }
    }

    public static void clear()
    {
        modifiers.clear();
        messages.clear();
    }

    public static List<String> get(String name)
    {
        return get(name, false);
    }

    public static double getModifier(String name)
    {
        return modifiers.get(name);
    }

    public static List<String> get(String name, boolean format)
    {
        if (format) {
            name = formatName(name);
        }

        // SenEnchants.getInstance().getChatManager().getLogger().log(Level.INFO, "Trying to find name: {0}", name);
        if (messages.containsKey(name)) {
            return messages.get(name);
        }
        return new ArrayList<>();
    }

    public static String getClassName(Class nclase)
    {
        String[] list = nclase.getName().split("\\.");
        String name = "";

        for (char character : list[list.length - 1].toCharArray()) {
            if (Character.isUpperCase(character)) {
                name += "_";
            }
            name += Character.toLowerCase(character);
        }
        return formatName(name.substring(1));
    }

    private static String formatName(String name)
    {
        String[] list = (name.toLowerCase()).split(" ");
        name = "";
        for (String part : list) {
            name += "_" + part;
        }
        return name.substring(1, name.length());
    }
}
