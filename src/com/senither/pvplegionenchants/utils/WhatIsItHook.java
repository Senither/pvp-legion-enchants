package com.senither.pvplegionenchants.utils;

import com.flobi.WhatIsIt.WhatIsIt;
import java.util.Map.Entry;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

public class WhatIsItHook
{

    public String itemName(ItemStack item)
    {
        return WhatIsIt.itemName(item);
    }

    public String entityName(Entity entity)
    {
        return WhatIsIt.entityName(entity);
    }

    public String blockName(Block block)
    {
        return WhatIsIt.blockName(block);
    }

    public String enchantmentName(Entry<Enchantment, Integer> enchantment)
    {
        return WhatIsIt.enchantmentName(enchantment);
    }
}
