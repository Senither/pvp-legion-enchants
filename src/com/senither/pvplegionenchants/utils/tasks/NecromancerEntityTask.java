package com.senither.pvplegionenchants.utils.tasks;

import org.bukkit.entity.Creature;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class NecromancerEntityTask extends BukkitRunnable
{

    private final Creature entity;
    private final Player target;
    private int runTime;

    public NecromancerEntityTask(Creature entity, Player target, int runTime, Plugin plugin)
    {
        this.entity = entity;
        this.target = target;
        this.runTime = runTime;
        runTaskTimer(plugin, 1L, 1L);
    }

    @Override
    public void run()
    {
        if (entity.isDead() || target.isDead() || runTime == 0) {
            if (!entity.isDead()) {
                entity.damage(1000D);
            }
            cancel();
            return;
        }

        entity.setTarget(target);
        runTime--;
    }
}
