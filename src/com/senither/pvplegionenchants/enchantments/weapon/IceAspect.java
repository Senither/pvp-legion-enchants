package com.senither.pvplegionenchants.enchantments.weapon;

import com.senither.pvplegionenchants.enchantments.armour.IceResistance;
import com.senither.pvplegionenchants.EnchantmentInterface;
import com.senither.pvplegionenchants.EnchantmentType;
import com.senither.pvplegionenchants.PvPLegionEnchants;
import com.senither.pvplegionenchants.utils.Messages;
import com.senither.pvplegionenchants.utils.effect.ParticleEffect;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

public class IceAspect implements EnchantmentInterface
{

    private String className;
    private final Random random = new Random();
    private final HashMap<String, Integer> players = new HashMap<>();

    @Override
    public List<String> getEnchantmentInformation()
    {
        return Messages.get(className);
    }

    @Override
    public double getModifier()
    {
        return Messages.getModifier(className);
    }

    @Override
    public EnchantmentType[] getType()
    {
        return new EnchantmentType[]{EnchantmentType.SWORD};
    }

    @Override
    public void playEffects(Location location)
    {
        Location l = new Location(location.getWorld(), location.getX(), location.getY(), location.getZ());
        ParticleEffect.SNOW_SHOVEL.display(0.5F, 0.5F, 0.5F, 0F, 42, l.add(0, 1.5, 0), 32F);
        ParticleEffect.SNOWBALL.display(0.5F, 0.5F, 0.5F, 0F, 42, l.add(0, 1.5, 0), 32F);
    }

    @Override
    public void register()
    {
        className = Messages.getClassName(getClass());
    }

    @Override
    public void run(Event event, ItemStack item, int level, String enchantmentString)
    {
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }

        if ((level * Messages.getModifier(className)) < (random.nextInt(100) + 1)) {
            return;
        }

        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;

        if (!(e.getEntity() instanceof Player)) {
            return;
        }

        final Player player = (Player) e.getEntity();

        playEffects(player.getLocation());

        if (players.containsKey(player.getName())) {
            PvPLegionEnchants.getInstance().getServer().getScheduler().cancelTask(players.get(player.getName()));
        }

        int time = 30;
        if (level == 2) {
            time = 40;
        } else if (level == 3) {
            time = 50;
        }

        if (IceResistance.iceResistance.containsKey(player.getName())) {
            int iceLevel = IceResistance.iceResistance.get(player.getName());
            boolean lowerSpeed = false;

            time = time - (iceLevel * 15);

            if (time <= 0) {
                time = 20;
                lowerSpeed = true;
            }

            if (lowerSpeed) {
                player.setWalkSpeed(0.15F);
            } else {
                player.setWalkSpeed(0.1F);
            }
        } else {
            player.setWalkSpeed(0.1F);
        }

        int scheduleSyncDelayedTask = PvPLegionEnchants.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(PvPLegionEnchants.getInstance(), new Runnable()
        {

            @Override
            public void run()
            {
                player.setWalkSpeed(0.2F);
                players.remove(player.getName());
            }
        }, time);
        players.put(player.getName(), scheduleSyncDelayedTask);
    }
}
