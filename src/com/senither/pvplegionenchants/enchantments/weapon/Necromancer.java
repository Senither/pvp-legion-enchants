package com.senither.pvplegionenchants.enchantments.weapon;

import com.senither.pvplegionenchants.EnchantmentInterface;
import com.senither.pvplegionenchants.EnchantmentType;
import com.senither.pvplegionenchants.PvPLegionEnchants;
import com.senither.pvplegionenchants.utils.Messages;
import com.senither.pvplegionenchants.utils.effect.ParticleEffect;
import com.senither.pvplegionenchants.utils.tasks.NecromancerEntityTask;
import java.util.List;
import java.util.Random;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Creature;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Necromancer implements EnchantmentInterface
{

    private String className;
    private final Random random = new Random();

    @Override
    public List<String> getEnchantmentInformation()
    {
        return Messages.get(className);
    }

    @Override
    public double getModifier()
    {
        return Messages.getModifier(className);
    }

    @Override
    public EnchantmentType[] getType()
    {
        return new EnchantmentType[]{EnchantmentType.SWORD};
    }

    @Override
    public void playEffects(Location location)
    {
    }

    @Override
    public void register()
    {
        className = Messages.getClassName(getClass());
    }

    @Override
    public void run(Event event, ItemStack item, int level, String enchantmentString)
    {
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }

        if ((level * Messages.getModifier(className)) < (random.nextInt(100) + 1)) {
            return;
        }

        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;

        if (!(e.getEntity() instanceof Player)) {
            return;
        }

        double chance = 0.50D;
        for (int i = 0; i < level; i++) {
            chance -= 0.02D;
        }

        if (chance <= 0.0D) {
            chance = 0.05D;
        }

        Player player = (Player) e.getEntity();
        Player damager = (Player) e.getDamager();

        double spawns = level * chance;
        spawns = Math.floor(spawns + 1.0D);

        int liveTime = 45 + (level * 5);

        ParticleEffect.SLIME.display(0.5F, 0.5F, 0.5F, 0F, 64, player.getLocation().add(0, 1.5, 0), 32F);

        for (int i = 0; i < spawns; i++) {
            Creature zombie = (Creature) player.getWorld().spawnEntity(player.getLocation(), EntityType.ZOMBIE);
            zombie.setCustomName(ChatColor.GRAY + damager.getName() + "'s Minion");
            zombie.setCustomNameVisible(true);
            zombie.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1000, 0));
            zombie.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 1000, 0));

            new NecromancerEntityTask(zombie, player, liveTime, PvPLegionEnchants.getInstance());
        }
    }
}
