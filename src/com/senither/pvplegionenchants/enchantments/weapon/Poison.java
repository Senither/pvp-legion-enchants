package com.senither.pvplegionenchants.enchantments.weapon;

import com.senither.pvplegionenchants.EnchantmentInterface;
import com.senither.pvplegionenchants.EnchantmentType;
import com.senither.pvplegionenchants.utils.Messages;
import com.senither.pvplegionenchants.utils.effect.ParticleEffect;
import java.util.List;
import java.util.Random;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Poison implements EnchantmentInterface
{

    private String className;
    private final Random random = new Random();

    @Override
    public List<String> getEnchantmentInformation()
    {
        return Messages.get(className);
    }

    @Override
    public double getModifier()
    {
        return Messages.getModifier(className);
    }

    @Override
    public EnchantmentType[] getType()
    {
        return new EnchantmentType[]{EnchantmentType.WEAPON};
    }

    @Override
    public void playEffects(Location location)
    {
        Location l = new Location(location.getWorld(), location.getX(), location.getY(), location.getZ());
        ParticleEffect.VILLAGER_HAPPY.display(0.5F, 0.5F, 0.5F, 0F, 64, l, 32F);
        ParticleEffect.VILLAGER_HAPPY.display(0.5F, 0.5F, 0.5F, 0F, 64, l.add(0, 1.5, 0), 32F);
    }

    @Override
    public void register()
    {
        className = Messages.getClassName(getClass());
    }

    @Override
    public void run(Event event, ItemStack item, int level, String enchantmentString)
    {
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }

        if ((level * Messages.getModifier(className)) < (random.nextInt(100) + 1)) {
            return;
        }

        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;

        if (!(e.getEntity() instanceof Player)) {
            return;
        }

        Player player = (Player) e.getEntity();

        int time;
        if (level == 1) {
            time = 60;
        } else if (level == 2) {
            time = 80;
        } else if (level == 3) {
            time = 90;
        } else {
            time = 110;
        }

        player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, time, ((level >= 3) ? 1 : 0)), true);
        playEffects(player.getLocation());
    }

}
