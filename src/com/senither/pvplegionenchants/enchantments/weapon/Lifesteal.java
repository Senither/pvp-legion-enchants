package com.senither.pvplegionenchants.enchantments.weapon;

import com.senither.pvplegionenchants.EnchantmentInterface;
import com.senither.pvplegionenchants.EnchantmentType;
import com.senither.pvplegionenchants.utils.Messages;
import com.senither.pvplegionenchants.utils.effect.ParticleEffect;
import java.util.List;
import java.util.Random;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

public class Lifesteal implements EnchantmentInterface
{

    private String className;
    private final Random random = new Random();

    @Override
    public List<String> getEnchantmentInformation()
    {
        return Messages.get(className);
    }

    @Override
    public double getModifier()
    {
        return Messages.getModifier(className);
    }

    @Override
    public EnchantmentType[] getType()
    {
        return new EnchantmentType[]{EnchantmentType.SWORD};
    }

    @Override
    public void playEffects(Location location)
    {
        Location l = new Location(location.getWorld(), location.getX(), location.getY(), location.getZ());
        ParticleEffect.REDSTONE.display(0.5F, 0.5F, 0.5F, 0F, 64, l.add(0, 1.5, 0), 32F);
    }

    @Override
    public void register()
    {
        className = Messages.getClassName(this.getClass());
    }

    @Override
    public void run(Event event, ItemStack item, int level, String enchantmentString)
    {
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }

        if ((level * Messages.getModifier(className)) < (random.nextInt(100) + 1)) {
            return;
        }

        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;

        // Used for when an arrow might call this event
        if (!(e.getDamager() instanceof Player)) {
            return;
        }

        Player player = (Player) e.getDamager();

        float precent = (level == 1) ? 10F : 20F;
        double add = e.getDamage() * (precent / 100F);

        double health = player.getHealth() + add;

        if (health > player.getMaxHealth()) {
            health = player.getMaxHealth();
        }

        if (level == 3 && player.getFoodLevel() < 20) {
            player.setFoodLevel(player.getFoodLevel() + 1);
        }

        player.setHealth(health);
        playEffects(player.getLocation());
    }
}
