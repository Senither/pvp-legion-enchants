package com.senither.pvplegionenchants.enchantments.armour;

import com.senither.pvplegionenchants.EnchantmentInterface;
import com.senither.pvplegionenchants.EnchantmentType;
import com.senither.pvplegionenchants.PvPLegionEnchants;
import com.senither.pvplegionenchants.utils.Messages;
import com.senither.pvplegionenchants.utils.effect.ParticleEffect;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

public class MoltenAura implements EnchantmentInterface, Runnable
{

    private String className;
    private int taskID = -1;
    public static final HashMap<String, Integer> moltenAura = new HashMap<>();
    private final Random random = new Random();

    @Override
    public List<String> getEnchantmentInformation()
    {
        return Messages.get(className);
    }

    @Override
    public double getModifier()
    {
        return Messages.getModifier(className);
    }

    @Override
    public EnchantmentType[] getType()
    {
        return new EnchantmentType[]{EnchantmentType.ARMOR};
    }

    @Override
    public void playEffects(Location location)
    {
        Location l = new Location(location.getWorld(), location.getX(), location.getY(), location.getZ());
        ParticleEffect.FLAME.display(0.45F, 0.45F, 0.45F, 0.03F, 8, l, 64F);
        ParticleEffect.FLAME.display(0.45F, 0.45F, 0.45F, 0.03F, 8, l.add(0, 1.2, 0), 64F);
        ParticleEffect.TOWN_AURA.display(0.2F, 0.2F, 0.2F, 0.04F, 16, l, 64F);

//        if (moltenAura.get(player.getName()) >= 5) {
//            ParticleEffect.LAVA.display(0.2F, 0.2F, 0.2F, 0.015F, 4, player.getLocation(), 64F);
//        }
    }

    @Override
    public void register()
    {
        className = Messages.getClassName(getClass());

        if (taskID != -1) {
            PvPLegionEnchants.getInstance().getServer().getScheduler().cancelTask(taskID);
        }
        taskID = PvPLegionEnchants.getInstance().getServer().getScheduler().scheduleSyncRepeatingTask(PvPLegionEnchants.getInstance(), this, 20, 40);
    }

    @Override
    public void run(Event event, ItemStack item, int level, String enchantmentString)
    {
        if (event instanceof InventoryCloseEvent) {
            InventoryCloseEvent e = (InventoryCloseEvent) event;
            String name = e.getPlayer().getName();

            // PvPLegionEnchants.getInstance().getLogger().log(Level.INFO, "InventoryCloseEvent was called for {0}, adding Molten Aura enchantment.", name);

            if (moltenAura.containsKey(name)) {
                moltenAura.remove(name);
            }
            moltenAura.put(name, level);
        } else if (event instanceof PlayerJoinEvent) {
            PlayerJoinEvent e = (PlayerJoinEvent) event;
            String name = e.getPlayer().getName();

            if (moltenAura.containsKey(name)) {
                moltenAura.remove(name);
            }
            moltenAura.put(name, level);
        } else if (event instanceof EntityDamageByEntityEvent) {
            try {
                EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;

                if ((level * Messages.getModifier(className)) < (random.nextInt(100) + 1)) {
                    return;
                }

                String name = ((Player) e.getEntity()).getName();
                e.getDamager().setFireTicks(25 * moltenAura.get(name));
            } catch (NullPointerException ex) {
                if (PvPLegionEnchants.getInstance().isDebugEnabled()) {
                    PvPLegionEnchants.getInstance().getLogger().log(Level.INFO, "NPE was called from a EntityDamageByEntityEvent event, message: {0}", ex.getLocalizedMessage());
                }
            }
        }
    }

    @Override
    public void run()
    {
        for (Player player : PvPLegionEnchants.getInstance().getServer().getOnlinePlayers()) {

            if (!moltenAura.containsKey(player.getName())) {
                continue;
            }

            playEffects(player.getLocation());
        }
    }
}
