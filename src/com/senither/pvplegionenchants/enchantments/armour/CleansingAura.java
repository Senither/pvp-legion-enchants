package com.senither.pvplegionenchants.enchantments.armour;

import com.senither.pvplegionenchants.EnchantmentInterface;
import com.senither.pvplegionenchants.EnchantmentType;
import com.senither.pvplegionenchants.PvPLegionEnchants;
import com.senither.pvplegionenchants.utils.Messages;
import com.senither.pvplegionenchants.utils.effect.ParticleEffect;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class CleansingAura implements EnchantmentInterface, Runnable
{

    private String className;
    private int taskID = -1;
    public static final HashMap<String, Integer> cleansingAura = new HashMap<>();
    private int countable = 0;

    @Override
    public List<String> getEnchantmentInformation()
    {
        return Messages.get(className);
    }

    @Override
    public double getModifier()
    {
        return Messages.getModifier(className);
    }

    @Override
    public EnchantmentType[] getType()
    {
        return new EnchantmentType[]{EnchantmentType.ARMOR};
    }

    @Override
    public void playEffects(Location location)
    {
        Location l = new Location(location.getWorld(), location.getX(), location.getY(), location.getZ());

        ParticleEffect.WATER_WAKE.display(0.45F, 0.45F, 0.45F, 0.03F, 16, l, 64F);
        ParticleEffect.WATER_SPLASH.display(0.45F, 0.45F, 0.45F, 0.03F, 32, l, 64F);

        ParticleEffect.WATER_WAKE.display(0.45F, 0.45F, 0.45F, 0.03F, 16, l.add(0, 0.5, 0), 64F);
        ParticleEffect.WATER_SPLASH.display(0.45F, 0.45F, 0.45F, 0.03F, 32, l.add(0, 0.5, 0), 64F);

        ParticleEffect.WATER_WAKE.display(0.45F, 0.45F, 0.45F, 0.03F, 16, l.add(0, 1.15, 0), 64F);
        ParticleEffect.WATER_SPLASH.display(0.45F, 0.45F, 0.45F, 0.03F, 32, l.add(0, 1.15, 0), 64F);

        ParticleEffect.WATER_WAKE.display(0.45F, 0.45F, 0.45F, 0.03F, 16, l.add(0, 1.7, 0), 64F);
        ParticleEffect.WATER_SPLASH.display(0.45F, 0.45F, 0.45F, 0.03F, 32, l.add(0, 1.7, 0), 64F);
    }

    @Override
    public void register()
    {
        className = Messages.getClassName(getClass());

        if (taskID != -1) {
            PvPLegionEnchants.getInstance().getServer().getScheduler().cancelTask(taskID);
        }
        taskID = PvPLegionEnchants.getInstance().getServer().getScheduler().scheduleSyncRepeatingTask(PvPLegionEnchants.getInstance(), this, 20, 5);
    }

    @Override
    public void run(Event event, ItemStack item, int level, String enchantmentString)
    {
        if (event instanceof InventoryCloseEvent) {
            InventoryCloseEvent e = (InventoryCloseEvent) event;
            String name = e.getPlayer().getName();

            if (cleansingAura.containsKey(name)) {
                cleansingAura.remove(name);
            }
            cleansingAura.put(name, level);
        } else if (event instanceof PlayerJoinEvent) {
            PlayerJoinEvent e = (PlayerJoinEvent) event;
            String name = e.getPlayer().getName();

            if (cleansingAura.containsKey(name)) {
                cleansingAura.remove(name);
            }
            cleansingAura.put(name, level);
        }
    }

    @Override
    public void run()
    {
        countable++;
        if (countable % 60 == 0) {
            // Level 1
            cleanseLevel(1);
        } else if (countable % 50 == 0) {
            // Level 2
            cleanseLevel(2);
        } else if (countable % 45 == 0) {
            // Level 3
            cleanseLevel(3);
        }
    }

    private void cleanseLevel(int level)
    {
        for (String name : cleansingAura.keySet()) {
            if (cleansingAura.get(name) == level) {
                Player player = PvPLegionEnchants.getInstance().getServer().getPlayer(name);
                if (player != null && player.isOnline()) {
                    boolean displayEffect = false;

                    if (player.hasPotionEffect(
                            PotionEffectType.SLOW)
                            || player.hasPotionEffect(PotionEffectType.BLINDNESS)
                            || player.hasPotionEffect(PotionEffectType.CONFUSION)
                            || player.hasPotionEffect(PotionEffectType.HARM)
                            || player.hasPotionEffect(PotionEffectType.HUNGER)
                            || player.hasPotionEffect(PotionEffectType.POISON)
                            || player.hasPotionEffect(PotionEffectType.SLOW)
                            || player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)
                            || player.hasPotionEffect(PotionEffectType.WEAKNESS)
                            || player.hasPotionEffect(PotionEffectType.WITHER)) {
                        displayEffect = true;
                    }

                    player.removePotionEffect(PotionEffectType.SLOW);
                    player.removePotionEffect(PotionEffectType.BLINDNESS);
                    player.removePotionEffect(PotionEffectType.CONFUSION);
                    player.removePotionEffect(PotionEffectType.HARM);
                    player.removePotionEffect(PotionEffectType.HUNGER);
                    player.removePotionEffect(PotionEffectType.POISON);
                    player.removePotionEffect(PotionEffectType.SLOW);
                    player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
                    player.removePotionEffect(PotionEffectType.WEAKNESS);
                    player.removePotionEffect(PotionEffectType.WITHER);

                    if (displayEffect) {
                        playEffects(player.getLocation());
                    }
                }
            }
        }
    }
}
