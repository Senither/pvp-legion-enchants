package com.senither.pvplegionenchants.enchantments.armour;

import com.senither.pvplegionenchants.EnchantmentInterface;
import com.senither.pvplegionenchants.EnchantmentType;
import com.senither.pvplegionenchants.PvPLegionEnchants;
import com.senither.pvplegionenchants.utils.Messages;
import com.senither.pvplegionenchants.utils.effect.ParticleEffect;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

public class IceResistance implements EnchantmentInterface, Runnable
{

    private String className;
    private int taskID = -1;
    public static final HashMap<String, Integer> iceResistance = new HashMap<>();

    @Override
    public List<String> getEnchantmentInformation()
    {
        return Messages.get(className);
    }

    @Override
    public double getModifier()
    {
        return Messages.getModifier(className);
    }

    @Override
    public EnchantmentType[] getType()
    {
        return new EnchantmentType[]{EnchantmentType.ARMOR};
    }

    @Override
    public void playEffects(Location location)
    {
        Location l = new Location(location.getWorld(), location.getX(), location.getY(), location.getZ());
        ParticleEffect.CRIT_MAGIC.display(0.45F, 0.45F, 0.45F, 0.03F, 8, l, 64F);
        ParticleEffect.CRIT_MAGIC.display(0.45F, 0.45F, 0.45F, 0.03F, 8, l.add(0, 0.6, 0), 64F);
        ParticleEffect.CRIT_MAGIC.display(0.45F, 0.45F, 0.45F, 0.03F, 8, l.add(0, 1.2, 0), 64F);
    }

    @Override
    public void register()
    {
        className = Messages.getClassName(getClass());

        if (taskID != -1) {
            PvPLegionEnchants.getInstance().getServer().getScheduler().cancelTask(taskID);
        }
        taskID = PvPLegionEnchants.getInstance().getServer().getScheduler().scheduleSyncRepeatingTask(PvPLegionEnchants.getInstance(), this, 20, 40);
    }

    @Override
    public void run(Event event, ItemStack item, int level, String enchantmentString)
    {
        if (event instanceof InventoryCloseEvent) {
            InventoryCloseEvent e = (InventoryCloseEvent) event;

            if (iceResistance.containsKey(e.getPlayer().getName())) {
                if (level >= iceResistance.get(e.getPlayer().getName())) {
                    iceResistance.put(e.getPlayer().getName(), level);
                }
                return;
            }
            iceResistance.put(e.getPlayer().getName(), level);
        } else if (event instanceof PlayerJoinEvent) {
            PlayerJoinEvent e = (PlayerJoinEvent) event;

            if (iceResistance.containsKey(e.getPlayer().getName())) {
                level += iceResistance.get(e.getPlayer().getName());
            }

            iceResistance.put(e.getPlayer().getName(), level);
        }
    }

    @Override
    public void run()
    {
        for (Player player : PvPLegionEnchants.getInstance().getServer().getOnlinePlayers()) {

            if (!iceResistance.containsKey(player.getName())) {
                continue;
            }

            playEffects(player.getLocation());
        }
    }
}
