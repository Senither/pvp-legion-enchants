package com.senither.pvplegionenchants;

import com.senither.pvplegionenchants.enchantments.armour.CleansingAura;
import com.senither.pvplegionenchants.enchantments.armour.IceResistance;
import com.senither.pvplegionenchants.enchantments.armour.MoltenAura;
import java.util.Arrays;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class EnchantmentManager
{

    public static ItemStack enchantItem(ItemStack item, Enchantments enchantment, int level)
    {
        EnchantmentType type = EnchantmentType.getType(item.getType());

        if (type == null) {
            return item;
        }

        EnchantmentType[] encType = enchantment.getClassName().getType();
        if (isValidEnchantment(encType, type)) {
            ItemMeta meta = item.getItemMeta();

            if (level > enchantment.getLevel()) {
                level = enchantment.getLevel();
            } else if (level < 1) {
                level = 1;
            }

            if (!meta.hasLore()) {
                meta.setLore(Arrays.asList(ChatColor.GRAY + enchantment.getName() + " " + intToEnchantmentLevel(level)));
            } else {
                List<String> enchants = meta.getLore();

                if (!isEnchanted(enchants, enchantment)) {
                    enchants.add(ChatColor.GRAY + enchantment.getName() + " " + intToEnchantmentLevel(level));
                } else {
                    for (int i = 0; i < enchants.size(); i++) {
                        if (ChatColor.stripColor(enchants.get(i)).startsWith(enchantment.getName())) {
                            enchants.set(i, ChatColor.GRAY + enchantment.getName() + " " + intToEnchantmentLevel(level));
                        }
                    }
                }

                meta.setLore(enchants);
            }

            item.setItemMeta(meta);
        }

        return item;
    }

    public static String intToEnchantmentLevel(int level)
    {
        if (level < 1) {
            level = 1;
        }

        switch (level) {
            case 1:
                return "I";
            case 2:
                return "II";
            case 3:
                return "III";
            case 4:
                return "IV";
            case 5:
                return "V";
            case 6:
                return "VI";
            case 7:
                return "VII";
            case 8:
                return "VIII";
            case 9:
                return "IX";
            case 10:
                return "X";
            default:
                return "I";
        }
    }

    public static int stringToEnchantmentLevel(String level)
    {
        switch (level) {
            case "I":
                return 1;
            case "II":
                return 2;
            case "III":
                return 3;
            case "IV":
                return 4;
            case "V":
                return 5;
            case "VI":
                return 6;
            case "VII":
                return 7;
            case "VIII":
                return 8;
            case "IX":
                return 9;
            case "X":
                return 10;
            default:
                return 1;
        }
    }

    public static void handelEnchantment(Event event, ItemStack item)
    {
        if (!item.hasItemMeta()) {
            return;
        }

        ItemMeta meta = item.getItemMeta();

        if (!meta.hasLore()) {
            return;
        }

        EnchantmentType type = EnchantmentType.getType(item.getType());
        if (type == null) {
            return;
        }

        for (String str : meta.getLore()) {
            Enchantments enchantment = getEnchantment(str);

            if (enchantment == null) {
                continue;
            }

            //EnchantmentType[] encTypes = enchantment.getClassName().getType();
            //if (isValidEnchantment(encTypes, type)) {
            String[] arr = str.split(" ");
            int level = stringToEnchantmentLevel(arr[arr.length - 1]);

            enchantment.getClassName().run(event, item, level, str);

            //}
        }
    }

    public static void removePlayer(Player player)
    {
        IceResistance.iceResistance.remove(player.getName());

        if (MoltenAura.moltenAura.containsKey(player.getName())) {
            MoltenAura.moltenAura.remove(player.getName());
        }
        if (CleansingAura.cleansingAura.containsKey(player.getName())) {
            CleansingAura.cleansingAura.remove(player.getName());
        }
    }

    private static Enchantments getEnchantment(String str)
    {
        for (Enchantments enchantment : Enchantments.values()) {
            if (ChatColor.stripColor(str).startsWith(enchantment.getName())) {
                return enchantment;
            }
        }
        return null;
    }

    private static boolean isEnchanted(List<String> enchants, Enchantments enchantment)
    {
        for (String enchant : enchants) {
            if (ChatColor.stripColor(enchant).startsWith(enchantment.getName())) {
                return true;
            }
        }
        return false;
    }

    private static boolean isValidEnchantment(EnchantmentType[] types, EnchantmentType check)
    {
        return true;
    }
}
