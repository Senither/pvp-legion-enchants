package com.senither.pvplegionenchants;

import com.senither.pvplegionenchants.enchantments.armour.CleansingAura;
import com.senither.pvplegionenchants.enchantments.armour.IceResistance;
import com.senither.pvplegionenchants.enchantments.armour.MoltenAura;
import com.senither.pvplegionenchants.enchantments.weapon.IceAspect;
import com.senither.pvplegionenchants.enchantments.weapon.Lifesteal;
import com.senither.pvplegionenchants.enchantments.weapon.Necromancer;
import com.senither.pvplegionenchants.enchantments.weapon.Poison;
import java.util.HashMap;
import java.util.Map;

public enum Enchantments
{

    LIFESTEAL(1, "Lifesteal", 3, new Lifesteal()),
    ICE_ASPECT(2, "Ice Aspect", 3, new IceAspect()),
    ICE_RESISTANCE(5, "Ice Resistance", 3, new IceResistance()),
    MOLTEN_AURA(5, "Molten Aura", 5, new MoltenAura()),
    CLEANSING_AURA(5, "Cleansing Aura", 3, new CleansingAura()),
    NECROMANCER(6, "Necromancer", 3, new Necromancer()),
    POISON(8, "Poison", 4, new Poison());

    private final int id;
    private final String name;
    private final int level;
    private final EnchantmentInterface className;
    private static final Map<String, Enchantments> map = new HashMap<>();

    static {
        for (Enchantments p : values()) {
            map.put(p.name.toLowerCase(), p);
        }
    }

    private Enchantments(int id, String name, int level, EnchantmentInterface className)
    {
        this.id = id;
        this.name = name;
        this.level = level;
        this.className = className;
    }

    /**
     * @return the id
     */
    public int getId()
    {
        return id;
    }

    /**
     * @return the level
     */
    public int getLevel()
    {
        return level;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     * @return the enchantment
     */
    public static Enchantments getEnchantmentByName(String name)
    {
        if (map.containsKey(name.toLowerCase())) {
            return map.get(name.toLowerCase());
        }
        return null;
    }

    /**
     * @return the className
     */
    public EnchantmentInterface getClassName()
    {
        return className;
    }

}
