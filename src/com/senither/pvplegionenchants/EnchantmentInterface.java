package com.senither.pvplegionenchants;

import java.util.List;
import org.bukkit.Location;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

public interface EnchantmentInterface
{

    public List<String> getEnchantmentInformation();
    
    public double getModifier();

    public EnchantmentType[] getType();

    public void register();

    public void run(Event event, ItemStack item, int level, String enchantmentString);
    
    public void playEffects(Location location);

}
