package com.senither.pvplegionenchants.listener;

import com.senither.pvplegionenchants.EnchantmentManager;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.ItemStack;

public class EntityListener implements Listener
{

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e)
    {
        if (e.getEntity() instanceof Player) {
            Player target = (Player) e.getEntity();
            for (ItemStack item : target.getInventory().getArmorContents()) {
                if (item == null || item.getType().equals(Material.AIR)) {
                    continue;
                }

                EnchantmentManager.handelEnchantment(e, item);
            }
        }

        Player player = null;
        if (e.getDamager() instanceof Arrow) {
            Arrow arrow = (Arrow) e.getDamager();
            if (arrow.getShooter() instanceof Player) {
                player = (Player) arrow.getShooter();
            }
        } else if (e.getDamager() instanceof Player) {
            player = (Player) e.getDamager();
        }

        if (player == null) {
            return;
        }

        if (player.getItemInHand() == null || player.getItemInHand().getType() == Material.AIR) {
            return;
        }

        EnchantmentManager.handelEnchantment(e, player.getItemInHand());
    }

    @EventHandler
    public void onEntityShotBow(EntityShootBowEvent e)
    {
        if ((e.getEntity() instanceof Player) && (e.getProjectile() instanceof Arrow)) {
            Player player = (Player) e.getEntity();

            if (player.getItemInHand() == null || player.getItemInHand().getType() == Material.AIR) {
                return;
            }

            EnchantmentManager.handelEnchantment(e, player.getItemInHand());
        }
    }

    @EventHandler
    public void onentityDeath(EntityDeathEvent e)
    {
        LivingEntity entity = e.getEntity();
        if (entity.getType().equals(EntityType.ZOMBIE) && e.getEntity().getName().contains("Minion")) {
            e.setDroppedExp(0);
            e.getDrops().clear();
        }
    }
}
