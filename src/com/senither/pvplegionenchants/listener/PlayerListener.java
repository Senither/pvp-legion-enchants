package com.senither.pvplegionenchants.listener;

import com.senither.pvplegionenchants.EnchantmentManager;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class PlayerListener implements Listener
{

    @EventHandler
    public void onPlayerCloseInventory(InventoryCloseEvent e)
    {
        //PvPLegionEnchants.getInstance().getLogger().log(Level.INFO, "Player closed inventory type: {0}", e.getInventory().getType());
        if (e.getInventory().getType() == InventoryType.CRAFTING) {
            PlayerInventory inventory = e.getPlayer().getInventory();

            EnchantmentManager.removePlayer((Player) e.getPlayer());

            if (e.getPlayer().getGameMode() != GameMode.CREATIVE) {
                Player player = (Player) e.getPlayer();
                player.setAllowFlight(false);
            }

            for (ItemStack item : inventory.getArmorContents()) {
                if (item == null) {
                    continue;
                }
                EnchantmentManager.handelEnchantment(e, item);
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e)
    {
        EnchantmentManager.removePlayer(e.getEntity());
    }
}
