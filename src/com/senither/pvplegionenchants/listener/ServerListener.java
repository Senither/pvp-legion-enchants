package com.senither.pvplegionenchants.listener;

import com.senither.pvplegionenchants.EnchantmentManager;
import com.senither.pvplegionenchants.PvPLegionEnchants;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class ServerListener implements Listener
{

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e)
    {
        for (ItemStack item : e.getPlayer().getInventory().getArmorContents()) {
            if (item == null) {
                continue;
            }
            EnchantmentManager.handelEnchantment(e, item);
        }

        final Player player = e.getPlayer();
        PvPLegionEnchants.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(PvPLegionEnchants.getInstance(), new Runnable()
        {
            @Override
            public void run()
            {
                if (player != null && player.isOnline() && player.getGameMode() != GameMode.CREATIVE && player.isFlying()) {
                    player.setAllowFlight(false);
                    player.setFlying(false);
                }
            }
        }, 30);
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent e)
    {
        EnchantmentManager.removePlayer(e.getPlayer());
    }
}
