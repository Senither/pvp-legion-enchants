package com.senither.pvplegionenchants.chat;

import com.senither.pvplegionenchants.PvPLegionEnchants;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class ChatManager
{

    private final PvPLegionEnchants plugin;
    private final Logger logger;
    private final ChatFancyText fancyText;
    private final String prefix;

    public ChatManager(PvPLegionEnchants plugin)
    {
        this.plugin = plugin;
        logger = plugin.getLogger();
        fancyText = new ChatFancyText();

        prefix = colorize("&8[&c&lPvPLegion&6&lEnchant&8] &7");
    }

    /**
     * @return the logger
     */
    public Logger getLogger()
    {
        return logger;
    }

    /**
     * @return the fancyText
     */
    public ChatFancyText getFancyText()
    {
        return fancyText;
    }

    /**
     * Colorize a message
     *
     * @param message The message
     */
    public String colorize(String message)
    {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    /**
     * Colorize a message
     *
     * @param messages The message
     */
    public List<String> colorize(List<String> messages)
    {
        List<String> message = new ArrayList<>();
        for (String str : messages) {
            if (str == null) {
                continue;
            }
            message.add(colorize(str));
        }
        return message;
    }

    /**
     * De-Colorize a message
     *
     * @param message The message
     */
    public String decolorize(String message)
    {
        return ChatColor.stripColor(message);
    }

    /**
     * Sends a message if the player is missing the permission
     *
     * @param player Player object
     * @param permission Permission string
     */
    public void missingPermission(Player player, String permission)
    {
        player.sendMessage(ChatColor.RED + "Influent permissions to execute this command.");
        player.sendMessage(ChatColor.RED + "You're missing the permission node " + ChatColor.ITALIC + permission);
    }

    /**
     * Send a message to a player or console
     *
     * @param player Command Sender object (Console)
     * @param message Message to send
     */
    public void sendMessage(CommandSender player, String message)
    {
        player.sendMessage(colorize(message));
    }

    /**
     * Send a message to a player or console
     *
     * @param player Command Sender object (Console)
     * @param message Message to send
     */
    public void sendPrefixMessage(CommandSender player, String message)
    {
        player.sendMessage(prefix + colorize(message));
    }

    public void broadcastMessage(String message)
    {
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            sendMessage(player, message);
        }
    }
}
