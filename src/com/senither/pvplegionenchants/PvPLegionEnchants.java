package com.senither.pvplegionenchants;

import com.senither.pvplegionenchants.listener.EntityListener;
import com.senither.pvplegionenchants.listener.PlayerListener;
import com.senither.pvplegionenchants.listener.ServerListener;
import com.senither.pvplegionenchants.chat.ChatFancyText;
import com.senither.pvplegionenchants.chat.ChatManager;
import com.senither.pvplegionenchants.utils.HookManager;
import com.senither.pvplegionenchants.utils.Messages;
import com.senither.pvplegionenchants.utils.Permissions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class PvPLegionEnchants extends JavaPlugin
{

    // Plugin instance
    private static PvPLegionEnchants instance;

    // Server variables
    private boolean debug = false;

    // Hooks
    private ChatManager chatManager;

    private final String[] enchantmentLevels = new String[]{"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"};

    @Override
    public void onEnable()
    {
        instance = this;

        saveDefaultConfig();
        debug = getConfig().getBoolean("debug-messages-enabled", false);

        Messages.register(this);

        // Setting up essential global variables
        chatManager = new ChatManager(this);

        if (debug) {
            chatManager.getLogger().info("Loading moduels..");
        }

        HookManager.register();
        Messages.register(this);

        if (debug) {
            chatManager.getLogger().info("Loading enchantments..");
        }

        for (Enchantments enchantment : Enchantments.values()) {
            enchantment.getClassName().register();
        }

        // Register events..
        if (debug) {
            chatManager.getLogger().info("Events - Registering event listeners");
        }
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new EntityListener(), this);
        pm.registerEvents(new ServerListener(), this);
        pm.registerEvents(new PlayerListener(), this);
    }

    @Override
    public void onDisable()
    {
        for (Player player : getServer().getOnlinePlayers()) {
            player.setWalkSpeed(0.2F);
        }
    }

    /**
     * @return the instance
     */
    public static PvPLegionEnchants getInstance()
    {
        return instance;
    }

    public boolean isDebugEnabled()
    {
        return debug;
    }

    public ChatManager getChatManager()
    {
        return chatManager;
    }

    /**
     * Enchants the given item stack with the given enchantment and level, if the level is 0 or lower, or over the max level of the enchantment, it will be corrected accordingly.
     *
     * @param item The item stack to enchant
     * @param enchantment The enchantment to put on the item stack
     * @param level The enchantments level
     * @return The enchanted item stack.
     */
    public ItemStack enchantItem(ItemStack item, Enchantments enchantment, int level)
    {
        return EnchantmentManager.enchantItem(item, enchantment, level);
    }

    /**
     * Gives you the enchantment information, this can be edited from the config provided with the PvPLegionEnchants plugin.
     *
     * @param enchantment
     * @return Arrat list of information.
     */
    public List<String> getEnchantmentInformation(Enchantments enchantment)
    {
        if (enchantment != null) {
            return enchantment.getClassName().getEnchantmentInformation();
        }
        return null;
    }

    /**
     * Plays the effect of a given enchantment at the given location.
     *
     * @param enchantment
     * @param location
     * @return true if the enchant exists, false otherwise.
     */
    public boolean playEffect(Enchantments enchantment, Location location)
    {
        if (enchantment != null) {
            enchantment.getClassName().playEffects(location);
            return true;
        }
        return false;
    }

    /**
     * Returns the items enchantment level for the given enchantment, if the user doesn't have the enchantment the method will return -1 instead, if the enchantment exists it will return it's integer.
     *
     * @param enchantment
     * @param item
     * @return -1 or the enchantment's current level on the ItemStack
     */
    public int getEnchantmentLevel(Enchantments enchantment, ItemStack item)
    {
        if (enchantment == null || item == null) {
            return -1;
        }

        ItemMeta meta = item.getItemMeta();
        if (!meta.hasLore()) {
            return -1;
        }

        List<String> enchants = meta.getLore();
        for (String enchant : enchants) {
            if (ChatColor.stripColor(enchant).startsWith(enchantment.getName())) {
                String[] size = ChatColor.stripColor(enchant).split(" ");
                String level = size[size.length - 1];

                if (Arrays.asList(enchantmentLevels).contains(level)) {
                    return EnchantmentManager.stringToEnchantmentLevel(level);
                }
            }
        }

        return -1;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You're using LegionEnchants v" + getDescription().getVersion());
            sender.sendMessage("Created by Alexis/Senither");
            return false;
        }

        Player player = (Player) sender;
        ChatFancyText msg = getChatManager().getFancyText();

        if (args.length == 0 || args[0].equalsIgnoreCase("help")) {
            if (!player.hasPermission(Permissions.USE)) {
                chatManager.missingPermission(player, Permissions.USE);
                return false;
            }

            msg.sendRawMessage(player.getName(), msg.getJsonMessage("&7[&6+&7]&m--&7[ &6&lLegionEnchants Help Menu &7]&7&m--&7[&6+&7]"));

            // List enchantments
            msg.sendRawMessage(player.getName(),
                    msg.getSuggestionMessage("&8/&7LEnchant &8<&blist&8>", "/lenchant list "),
                    msg.getJsonMessage("&f: List all the enchants")
            );

            // Reload
            msg.sendRawMessage(player.getName(),
                    msg.getSuggestionMessage("&8/&7LEnchant &8<&areload&8>", "/lenchant reload "),
                    msg.getJsonMessage("&f: Reloads the plugin")
            );

            // List enchantments
            msg.sendRawMessage(player.getName(),
                    msg.getSuggestionMessage("&8/&7LEnchant &8<&eenchantment&8> &8<&elevel&8>", "/lenchant enchantment level "),
                    msg.getJsonMessage("&f: Enchant an item")
            );
            return true;
        } else if (args[0].equalsIgnoreCase("list")) {
            if (!player.hasPermission(Permissions.LIST)) {
                chatManager.missingPermission(player, Permissions.LIST);
                return false;
            }
            msg.sendRawMessage(player.getName(), msg.getJsonMessage("&7[&b+&7]&m--&7[ &b&lLegionEnchants List &7]&7&m--&7[&b+&7]"));

            ChatFancyText text = chatManager.getFancyText();

            int count = 0;
            String[] json = new String[Enchantments.values().length * 2];
            for (Enchantments enchantment : Enchantments.values()) {
                ItemStack item = new ItemStack(Material.STONE);
                ItemMeta meta = item.getItemMeta();

                List<String> info = new ArrayList<>(enchantment.getClassName().getEnchantmentInformation());

                if (info.isEmpty()) {
                    meta.setDisplayName(PvPLegionEnchants.getInstance().getChatManager().colorize("&f&l" + enchantment.getName()));
                    meta.setLore(PvPLegionEnchants.getInstance().getChatManager().colorize(Arrays.asList("&7This enchantment have not been setup yet!", "&7Please create it in the config.")));
                } else {
                    meta.setDisplayName(PvPLegionEnchants.getInstance().getChatManager().colorize(info.get(0)));
                    info.remove(0);

                    List<String> modifiers = new ArrayList<>();
                    modifiers.add("");
                    modifiers.add("&bModifier&f: &r &7 &r&7" + enchantment.getClassName().getModifier());
                    modifiers.add("&bMax Level&f: &7" + enchantment.getLevel());
                    modifiers.add("");

                    for (String line : info) {
                        modifiers.add(line);
                    }

                    meta.setLore(PvPLegionEnchants.getInstance().getChatManager().colorize(modifiers));
                }
                item.setItemMeta(meta);

                json[count++] = text.getItemMessage("&7" + enchantment.getName().replace(" ", "_"), item);
                json[count++] = text.getJsonMessage("&8, ");
            }

            json[json.length - 1] = null;

            text.sendRawMessage(player.getName(), json);
            return true;
        } else if (args[0].equalsIgnoreCase("reload")) {
            if (!player.hasPermission(Permissions.RELOAD)) {
                chatManager.missingPermission(player, Permissions.RELOAD);
                return false;
            }

            getChatManager().sendMessage(player, "&8[&aツ&8]&m-&8[ &aReloading Legion Enchants..");

            reloadConfig();
            debug = getConfig().getBoolean("debug-messages-enabled", false);
            Messages.register(this);

            getChatManager().sendMessage(player, "&8[&aツ&8]&m-&8[ &aLegion Enchants v" + getDescription().getVersion() + " has been reloaded successfully!");

        } else if (args[0].equalsIgnoreCase("author")) {
            chatManager.sendMessage(player, "&eLegionEnchants &6was developed by &eAlexis &6/ &eSenither");
            chatManager.sendMessage(player, "&6Plugin Version: &e" + getDescription().getVersion());
        } else {
            String enchantmentName = args[0].replace("_", " ");
            int level = 1;

            Enchantments enchantment = Enchantments.getEnchantmentByName(enchantmentName);
            if (enchantment == null) {
                chatManager.sendMessage(sender, "&7[&c-&7]&m--&7[ &cUnknown enchant, \"" + enchantmentName + "\" was not found!");
                return true;
            }

            if (args.length >= 2) {
                try {
                    level = Integer.parseInt(args[1]);
                } catch (NumberFormatException e) {
                    chatManager.sendMessage(player, "&7[&c-&7]&m--&7[ &cInvalid enchantment level! Please use an integer.");
                    return true;
                }
            }

            if (player.getItemInHand() == null || player.getItemInHand().getType().equals(Material.AIR)) {
                chatManager.sendMessage(player, "&7[&c-&7]&m--&7[ &cYou have nothing in your hand");
                return true;
            }

            EnchantmentType type = EnchantmentType.getType(player.getItemInHand().getType());

            if (type == null) {
                chatManager.sendMessage(player, "&7[&c-&7]&m--&7[ &cThe enchantment can not be applied to your itemstack!");
                return true;
            }

            EnchantmentManager.enchantItem(player.getItemInHand(), enchantment, level);
            chatManager.sendMessage(player, "&7[&e+&7]&m--&7[ &6" + enchantment.getName() + " &ehas been applied to your itemstack!");
        }
        return false;
    }
}
